<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\BigFootSighting;
use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

final class AppFixtures extends Fixture
{
    private Generator $faker;
    private ObjectManager $objectManager;
    private UserPasswordEncoderInterface $passwordEncoder;

    /** @var User[] */
    private array $users = [];
    /** @var BigFootSighting[] */
    private array $sightings = [];

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager): void
    {
        $this->objectManager = $manager;
        $this->faker = Factory::create();

        $this->createUsers();
        $this->createSightings();
        $this->createComments();

        $manager->flush();
    }

    private function createUsers(): void
    {
        $this->users = $this->createMany(
            5,
            function () {
                $user = new User();
                $user->setUsername($this->faker->userName);
                $user->setEmail($user->getUsername().'@example.com');
                $user->setPassword(
                    $this->passwordEncoder->encodePassword($user, 'believe')
                );
                $user->setAgreedToTermsAt($this->faker->dateTimeBetween('-6 months'));

                return $user;
            }
        );
    }

    /**
     * @return array<mixed>
     */
    private function createMany(int $amount, callable $callback): array
    {
        $objects = [];
        for ($i = 0; $i < $amount; ++$i) {
            $object = $callback($i);
            $this->objectManager->persist($object);

            $objects[] = $object;
        }
        $this->objectManager->flush();

        return $objects;
    }

    private function createSightings(): void
    {
        $this->sightings = $this->createMany(
            50,
            function () {
                $sighting = new BigFootSighting();
                $sighting->setOwner($this->users[array_rand($this->users)]);
                $sighting->setTitle($this->faker->realText(80));
                $sighting->setDescription($this->faker->paragraph);
                $sighting->setScore(random_int(1, 10));
                $sighting->setLatitude((string) $this->faker->latitude);
                $sighting->setLongitude((string) $this->faker->longitude);
                $sighting->setCreatedAt($this->faker->dateTimeBetween('-6 months'));

                return $sighting;
            }
        );
    }

    private function createComments(): void
    {
        $this->createMany(
            200,
            function (int $i) {
                $comment = new Comment();
                if (0 === $i % 5) {
                    // make every 5th comment done by a small set of users
                    // Wow! They must *love* Bigfoot!
                    $rangeMax = (int) floor(count($this->users) / 10);
                    $comment->setOwner($this->users[random_int(0, $rangeMax)]);
                } else {
                    $comment->setOwner($this->users[array_rand($this->users)]);
                }
                $bigFootSighting = $this->sightings[array_rand($this->sightings)];
                $comment->setBigFootSighting($bigFootSighting);
                $comment->setContent($this->faker->paragraph);
                $comment->setCreatedAt($this->faker->dateTimeBetween($bigFootSighting->getCreatedAt()));

                return $comment;
            }
        );
    }
}
