<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\BigFootSighting;
use App\Model\BigFootSightingScore;
use App\Scoring\ScoreAdjusterInterface;
use App\Scoring\ScoringFactorInterface;

class SightingScorer
{
    /**
     * @var ScoreAdjusterInterface[]|iterable
     */
    private iterable $scoringAdjusters;
    /**
     * @var ScoringFactorInterface[]
     */
    private iterable $scoringFactors;

    /**
     * @param ScoringFactorInterface[] $scoringFactors
     * @param ScoreAdjusterInterface[] $scoringAdjusters
     */
    public function __construct(iterable $scoringFactors, iterable $scoringAdjusters)
    {
        $this->scoringAdjusters = $scoringAdjusters;
        $this->scoringFactors = $scoringFactors;
    }

    public function score(BigFootSighting $sighting): BigFootSightingScore
    {
        $score = 0;
        foreach ($this->scoringFactors as $scoringFactor) {
            $score += $scoringFactor->score($sighting);
        }
        foreach ($this->scoringAdjusters as $scoringAdjuster) {
            $score = $scoringAdjuster->adjustScore($score, $sighting);
        }

        return new BigFootSightingScore($score);
    }
}
