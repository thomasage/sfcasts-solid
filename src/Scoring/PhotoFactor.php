<?php

declare(strict_types=1);

namespace App\Scoring;

use App\Entity\BigFootSighting;

final class PhotoFactor implements ScoreAdjusterInterface, ScoringFactorInterface
{
    public function adjustScore(int $finalScore, BigFootSighting $sighting): int
    {
        $photosCount = count($sighting->getImages());
        if ($finalScore < 50 && $photosCount > 2) {
            $finalScore += $photosCount * 5;
        }

        return $finalScore;
    }

    public function score(BigFootSighting $sighting): int
    {
        if (0 === count($sighting->getImages())) {
            return 0;
        }
        $score = 0;
        foreach ($sighting->getImages() as $image) {
            $score += random_int(1, 100); // todo analyze image
        }

        return $score;
    }
}
